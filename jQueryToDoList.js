$( document ).ready(function() {
    /**
   * Toggles "done" class on <li> element
   */
  $('li').click(function(e){
    let $this = $(this);
    $this.toggleClass('done');
  })

  /**
   * Delete element when delete link clicked
   */

  $('.delete').click(function(e){
    let $this = $(this);
    $this.parents()[0].remove();
  })

  /**
   * Adds new list item to <ul>
   */
  const addListItem = function(e) {
    const text = $('input').val();
    $('.today-list').append(
      $('<li>').append(
        $('<span>').append(text),
      $('<a>').attr('class','delete').append('Delete'))
    )
    
    //add listener for done toggle
    $('li').click(function(e){
      let $this = $(this);
      $this.toggleClass('done');
    })
    
    //add listener for delete
    $('.delete').click(function(e){
      let $this = $(this);
      $this.parents()[0].remove();
    })

  };

  // add listener for add
  $('.add-item').click(function(e){
    addListItem(e);
  })
});
