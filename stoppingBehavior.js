// Do not change
document.getElementById('cat').addEventListener('click', () => {
  alert('meow!');
});

// When clicked, "More info" link should alert "Here's some info"
// instead of going to a new webpage
$('a').click(function(e) {
  e.preventDefault();
  alert('Here\'s some info');
})

// When the bark button is clicked, should alert "Bow wow!"
// Should *not* alert "meow"

$('button').click(function(e) {
  e.preventDefault();
  alert('Bow wow!');
  return false; // took awhile to find this/figure it out.
  //without it bubbling still happened.  e.stopPropogation() doesn't work
  //here for some reason.
})
